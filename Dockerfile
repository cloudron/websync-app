FROM cloudron/base:0.3.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y
RUN apt-get install -y software-properties-common build-essential openssl libreadline6 libreadline6-dev curl git-core zlib1g zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt-dev autoconf libc6-dev ncurses-dev automake libtool bison subversion pkg-config wget python-software-properties python python-setuptools libpq5 libpq-dev nodejs unoconv libhiredis-dev poppler-utils libreoffice-core libreoffice-calc libreoffice-writer libreoffice-impress nginx npm openjdk-7-jre-headless libmagic-dev postgresql-server-dev-9.4 postgresql-client-9.4

RUN gem install rake
RUN npm install -g js-beautify docco bower grunt-cli pm2

RUN mkdir -p /app/code; mkdir -p /app/home;
RUN chown -R daemon:daemon /app/code; chown -R daemon:daemon /app/home; usermod -d /app/home daemon; chown -R daemon:daemon /var/log/nginx; chown -R daemon:daemon /var/lib/nginx;

# allow nginx to create a PID file as daemon user
RUN mkdir -p /run
RUN chmod a+w /run

ENV HOME /app/home
USER daemon

WORKDIR /app/code

RUN curl -L https://github.com/d4l3k/WebSync/archive/master.tar.gz | tar -xz --strip-components 1 -f  -

ADD Gemfile /app/code/Gemfile
ADD Gemfile.lock /app/code/Gemfile.lock

# Download dependencies
RUN bundle install --deployment
RUN npm install
RUN bower install
RUN bundle exec rake assets:precompile

ADD omniauth-providers.rb /app/code/config/omniauth-providers.rb
ADD nginx.conf /app/code/config/nginx.conf
ADD start.sh /app/code/start.sh

# will get expanded in start.sh
ADD config.json_template /app/code/config.json_template

EXPOSE 3000

CMD [ "/app/code/start.sh" ]
