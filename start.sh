#!/bin/sh

set -eu

readonly APP_DIR="/app/code"
readonly fqdn=$(hostname -f)

cd "$APP_DIR"

sed -e "s/##HOSTNAME/${fqdn}/g" \
    -e "s/##POSTGRESQL_USERNAME/${POSTGRESQL_USERNAME}/g" \
    -e "s/##POSTGRESQL_PASSWORD/${POSTGRESQL_PASSWORD}/g" \
    -e "s/##POSTGRESQL_HOST/${POSTGRESQL_HOST}/g" \
    -e "s/##POSTGRESQL_PORT/${POSTGRESQL_PORT}/g" \
    -e "s/##POSTGRESQL_DATABASE/${POSTGRESQL_DATABASE}/g" \
    -e "s/##REDIS_PORT/${REDIS_PORT}/g" \
    -e "s/##REDIS_HOST/${REDIS_HOST}/g" \
    -e "s/##REDIS_PASSWORD/${REDIS_PASSWORD}/g" \
    "${APP_DIR}/config.json_template" > "${APP_DIR}/config/config.json"

nginx -c "${APP_DIR}/config/nginx.conf" -p "${APP_DIR}/config"

pm2 start bin/backend.js
bundle exec rackup
